﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace algorithms_tests
{
    [TestClass]
    public class ArraySearchAlgorithmTests
    {
        [TestMethod]
        public void BinarySearch_values_size_find_value_nearest_pos()
        {
            int[] values = { 2, 3, 5, 6, 7, 9, 10, 13, 14, 16 };
            int[] invalid_search_values = { -1, 0, 1, 4, 8, 11, 12, 15, 17, 18 };
            Int64 nearest_pos = 0;

            bool found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, 1, out nearest_pos);
            Assert.IsFalse(found);
            Assert.AreEqual(nearest_pos, 0);

            found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, 4, out nearest_pos);
            Assert.IsFalse(found);

            for (int pos = 0; pos < values.Length; pos++)
            {
                int value_to_find = values[pos];
                found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, value_to_find, out nearest_pos);
                Assert.IsTrue(found);
                Assert.AreEqual(pos, nearest_pos);
            }
        }

        [TestMethod]
        public void BinarySearch_values_size_find_value()
        {
            int[] values = { 2, 3, 5, 6, 7, 9, 10, 13, 14, 16 };
            int[] invalid_search_values = { -1, 0, 1, 4, 8, 11, 12, 15, 17, 18 };

            bool found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, 1);
            Assert.IsFalse(found);

            found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, 4);
            Assert.IsFalse(found);

            for (int pos = 0; pos < values.Length; pos++)
            {
                int value_to_find = values[pos];
                found = algorithms.ArraySearchAlgorithm<int>.BinarySearch(values, value_to_find);
                Assert.IsTrue(found);
            }
        }
    }
}
