﻿/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorithms
{
    static public class ArraySearchAlgorithm<Type> where Type : struct, System.IComparable<Type>
    {
        static public bool BinarySearch(Type[] values,
                                        Type value_to_find,
                                        out Int64 nearest_pos)
        {
            bool success;

            if (values.Length > 0)
            {
                Int64 start = 0;
                Int64 end = values.Length - 1;
                Int64 mid;

                do
                {
                    mid = (start + end) / 2;
                    Type to_compare = values[mid];
                    int cmp = value_to_find.CompareTo(to_compare);
                    if (cmp < 0)
                        end = mid - 1;
                    else if (cmp > 0)
                        start = mid + 1;
                    else
                        break;
                }
                while (start <= end);

                nearest_pos = mid;
                success = start <= end;
            }
            else
            {
                nearest_pos = 0;
                success = false;
            }

            return success;
        }

        static public bool BinarySearch(Type[] values,
                                        Type value_to_find)
        {
            bool success;

            if (values.Length > 0)
            {
                Int64 start = 0;
                Int64 end = values.Length - 1;
                Int64 mid;

                do
                {
                    mid = (start + end) / 2;
                    Type to_compare = values[mid];
                    int cmp = value_to_find.CompareTo(to_compare);
                    if (cmp < 0)
                        end = mid - 1;
                    else if (cmp > 0)
                        start = mid + 1;
                    else
                        break;
                }
                while (start <= end);

                success = start <= end;
            }
            else
                success = false;

            return success;
        }
    }
}
